package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "bitbucket.org/actions/actions-go/pkg/packages/hello"
	_ "bitbucket.org/actions/actions-go/pkg/packages/html"
	_ "bitbucket.org/actions/actions-go/pkg/packages/http"
	"bitbucket.org/actions/actions-go/pkg/remote"
	"bitbucket.org/actions/actions-go/pkg/services"
)

func methodCall(w http.ResponseWriter, r *http.Request) {
	// Get name from request query
	name := r.URL.Query().Get("name")

	// Read request if method is POST
	var request services.M
	if r.Method == "POST" {
		err := json.NewDecoder(r.Body).Decode(&request)
		if err != nil {
			remote.WriteError(w, http.StatusBadRequest, err)
			return
		}
	}

	// Create response map
	response := make(services.M)

	// Call service
	err := services.Call(name, request, response)
	if err != nil {
		remote.WriteError(w, http.StatusInternalServerError, err)
		return
	}

	// Write response
	writeResponse(w, response)
}

func sequenceCall(w http.ResponseWriter, r *http.Request) {
	// Read sequence from request body
	var seq services.Sequence
	err := json.NewDecoder(r.Body).Decode(&seq)
	if err != nil {
		remote.WriteError(w, http.StatusBadRequest, err)
		return
	}

	// Create response map
	response := make(services.M)

	// Call service
	err = services.RunSequence(seq, nil, response)
	if err != nil {
		remote.WriteError(w, http.StatusInternalServerError, err)
		return
	}

	// Write response
	writeResponse(w, response)
}

func writeResponse(w http.ResponseWriter, response interface{}) {
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(response)
	if err != nil {
		remote.WriteError(w, http.StatusInternalServerError, err)
		return
	}
}

func main() {
	http.HandleFunc("/", methodCall)
	http.HandleFunc("/seq", sequenceCall)

	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "ok")
	})

	log.Fatal(http.ListenAndServe(":8000", nil))
}
