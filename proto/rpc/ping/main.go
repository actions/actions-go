package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func main() {
	start := time.Now()

	resp, err := http.Get("http://127.0.0.1:8000/ping")
	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Remote call response: %s", body)
	log.Printf("time: %v", time.Now().Sub(start))
}
