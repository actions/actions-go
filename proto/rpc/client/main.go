package main

import (
	"encoding/json"
	"log"
	"os"
	"time"

	"bitbucket.org/actions/actions-go/pkg/remote"
	"bitbucket.org/actions/actions-go/pkg/services"
)

func main() {
	var filename string
	if len(os.Args) <= 1 {
		log.Fatal("Usage: client <filename>")
	} else {
		filename = os.Args[1]
	}

	seq, err := readSequence(filename)
	if err != nil {
		log.Fatal(err)
	}

	start := time.Now()
	remote.Register("http://127.0.0.1:8000/")

	// Call remote service
	var response services.M
	err = remote.RunSequence(seq, nil, &response)
	if err != nil {
		log.Fatal(err)
	}

	b, err := json.MarshalIndent(response, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Remote call response: \n%s", b)
	log.Printf("time: %v", time.Now().Sub(start))
}

func readSequence(filename string) (seq services.Sequence, err error) {
	// Read api file
	file, err := os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()

	// Unmarshal json API
	err = json.NewDecoder(file).Decode(&seq)
	return
}
