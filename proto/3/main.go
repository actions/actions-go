package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "bitbucket.org/actions/actions-go/pkg/packages/hello"
	_ "bitbucket.org/actions/actions-go/pkg/packages/html"
	_ "bitbucket.org/actions/actions-go/pkg/packages/http"
	"bitbucket.org/actions/actions-go/pkg/services"
)

func main() {
	http.HandleFunc("/run", func(w http.ResponseWriter, r *http.Request) {
		var s services.Sequence
		err := json.NewDecoder(r.Body).Decode(&s)
		if err != nil {
			writeError(w, err)
			return
		}

		response := make(services.M)
		err = services.RunSequence(s, reqArgs(r), response)
		if err != nil {
			writeError(w, err)
			return
		}

		err = json.NewEncoder(w).Encode(response)
		if err != nil {
			writeError(w, err)
			return
		}
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Get name from request query
		name := r.URL.Query().Get("name")

		response := make(services.M)
		err := services.Call(name, nil, response)
		if err != nil {
			writeError(w, err)
			return
		}

		err = json.NewEncoder(w).Encode(response)
		if err != nil {
			writeError(w, err)
			return
		}
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "<title>Hello world!</h1>")
	})

	log.Fatal(http.ListenAndServe(":8000", nil))
}

func reqArgs(r *http.Request) services.M {
	q := r.URL.Query()
	if len(q) == 0 {
		return nil
	}

	args := make(services.M)
	for key := range q {
		args[key] = q.Get(key)
	}
	return args
}

func writeError(w http.ResponseWriter, err error) {
	json.NewEncoder(w).Encode(struct {
		Error string `json:"error"`
	}{Error: err.Error()})
}
