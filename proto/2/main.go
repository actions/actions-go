package main

import (
	"log"

	_ "bitbucket.org/actions/actions-go/pkg/packages/html"
	_ "bitbucket.org/actions/actions-go/pkg/packages/http"
	"bitbucket.org/actions/actions-go/pkg/services"
	_ "bitbucket.org/actions/actions-go/proto/1/svc"
)

func main() {
	var titleResponse = struct{ Title string }{}
	var seq = services.Sequence{
		{"http.request", services.M{"URL": "http://google.com/"}},
		{"html.extract", services.M{"Selectors": services.M{"Title": "//title/text()"}}},
	}

	// Run sequence
	err := services.RunSequence(seq, services.M{"URL": "http://youtube.com/"}, &titleResponse)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Got title: %q", titleResponse.Title)
}
