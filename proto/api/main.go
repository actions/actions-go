package main

import (
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"os"

	"bitbucket.org/actions/actions-go/pkg/apis"
	_ "bitbucket.org/actions/actions-go/pkg/packages/hello"
	_ "bitbucket.org/actions/actions-go/pkg/packages/html"
	_ "bitbucket.org/actions/actions-go/pkg/packages/http"
	_ "bitbucket.org/actions/actions-go/pkg/packages/json"
)

var addr = flag.String("addr", ":9000", "API listening address")
var filename = flag.String("api", "api.json", "API JSON file")

func main() {
	flag.Parse()
	api := new(apis.API)

	// Read api file
	file, err := os.Open(*filename)
	if err != nil {
		log.Fatal(err)
	}

	// Unmarshal json API
	err = json.NewDecoder(file).Decode(api)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()

	server := &http.Server{
		Addr:    *addr,
		Handler: api,
	}

	log.Printf("Starting server on %s", *addr)

	log.Fatal(server.ListenAndServe())
}
