package main

import (
	"log"
	"time"

	"bitbucket.org/actions/actions-go/pkg/services"
	_ "bitbucket.org/actions/actions-go/proto/1/svc"
)

type m map[string]interface{}
type myResponse struct{ Time time.Time }

func main() {
	req := m{"TimeZone": "Europe/Warsaw"}
	resp := &myResponse{}

	err := services.Call("time.now", req, resp)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("resp: %s", resp.Time)
}
