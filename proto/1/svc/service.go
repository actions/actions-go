package svc

import (
	"time"

	"bitbucket.org/actions/actions-go/pkg/services"
)

// TimeRequest -
type TimeRequest struct {
	TimeZone string
}

// TimeResponse -
type TimeResponse struct {
	Time time.Time
}

// MyService - Test service.
type MyService struct{}

// Now -
func (s *MyService) Now(req *TimeRequest, resp *TimeResponse) error {
	resp.Time = time.Now()
	if req.TimeZone != "" {
		loc, err := time.LoadLocation(req.TimeZone)
		if err != nil {
			return err
		}
		resp.Time = resp.Time.In(loc)
	}
	return nil
}

func init() {
	services.Register(&MyService{}, "time", "v1", "Time Service")
}
