package remote

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"

	"bitbucket.org/actions/actions-go/pkg/services"
)

// Registry - Registry of remote services.
type Registry struct {
	hosts []string
}

// NewRegistry - Creates a new remote registry.
func NewRegistry(hosts ...string) *Registry {
	return &Registry{hosts: hosts}
}

// Register - Registers remote service.
func (r *Registry) Register(host string) {
	r.hosts = append(r.hosts, host)
}

// Call - Calls remote service method.
func (r *Registry) Call(name string, req, resp interface{}) (err error) {
	// Service url
	uri := fmt.Sprintf("%s?name=%s", r.host(), name)

	// Create a http request
	var response *http.Response
	if req == nil {
		response, err = http.Get(uri)
	} else {
		var buf bytes.Buffer
		err = json.NewEncoder(&buf).Encode(req)
		if err != nil {
			return
		}
		response, err = http.Post(uri, "application/json", &buf)
	}
	if err != nil {
		return
	}

	// Read error if status is not ok
	if response.StatusCode != http.StatusOK {
		e := &Error{}
		err = json.NewDecoder(response.Body).Decode(e)
		if err != nil {
			return
		}
		return e
	}

	// Decode response
	err = json.NewDecoder(response.Body).Decode(resp)
	if err != nil {
		return
	}

	return
}

// RunSequence - Runs sequence remotely.
// TODO: sequence args
func (r *Registry) RunSequence(seq services.Sequence, req, resp interface{}) (err error) {
	// Service sequence endpoint
	uri := fmt.Sprintf("%sseq", r.host())

	// Create a http request
	var buf bytes.Buffer
	var response *http.Response
	err = json.NewEncoder(&buf).Encode(seq)
	if err != nil {
		return
	}

	// Request sequence run
	response, err = http.Post(uri, "application/json", &buf)
	if err != nil {
		return
	}

	// Read error if status is not ok
	if response.StatusCode != http.StatusOK {
		e := &Error{}
		err = json.NewDecoder(response.Body).Decode(e)
		if err != nil {
			return
		}
		return e
	}

	// Decode response
	err = json.NewDecoder(response.Body).Decode(resp)
	if err != nil {
		return
	}

	return
}

// TODO: run sequence
// TODO: run remote actions if not locally found

// host - Gets a random host.
func (r *Registry) host() string {
	if len(r.hosts) == 0 {
		return ""
	}

	return r.hosts[rand.Intn(len(r.hosts))]
}
