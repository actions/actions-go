package remote

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Error - Call error.
type Error struct {
	Code    int    `json:"error_code,omitempty"`
	Message string `json:"error_message"`
}

var errorsNames = map[int]string{
	http.StatusInternalServerError: "Internal Server Error",
	http.StatusBadRequest:          "Bad Request",
	http.StatusUnauthorized:        "Unauthorized",
	http.StatusForbidden:           "Forbidden",
	http.StatusNotFound:            "Not Found",
	http.StatusConflict:            "Conflict",
}

// NewError - Creates new error.
func NewError(message string, status int) *Error {
	if name, ok := errorsNames[status]; ok {
		message = fmt.Sprintf("%s: %s", name, message)
	}

	return &Error{
		Code:    status,
		Message: message,
	}
}

// WriteError - Writes http error.
func WriteError(w http.ResponseWriter, status int, err error) {
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(NewError(err.Error(), status))
}

// Error - Returns error message.
func (e *Error) Error() string {
	return e.Message
}
