package remote

import "bitbucket.org/actions/actions-go/pkg/services"

// DefaultRegistry - Default remote services registry.
var DefaultRegistry = NewRegistry()

// Call - Calls remote service method.
func Call(name string, req, resp interface{}) error {
	return DefaultRegistry.Call(name, req, resp)
}

// RunSequence - Runs sequence remotely.
func RunSequence(seq services.Sequence, req, resp interface{}) error {
	return DefaultRegistry.RunSequence(seq, req, resp)
}

// Register - Registers remote service.
func Register(host string) {
	DefaultRegistry.Register(host)
}
