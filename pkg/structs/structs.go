package structs

import "reflect"

// Convert - Converts source to another type.
// Source can be a struct or a map.
func Convert(source interface{}, t reflect.Type) (dest reflect.Value) {
	// If type is a pointer, get it's element type.
	kind := t.Kind()
	if kind == reflect.Ptr {
		t = t.Elem()
	}

	// If source is empty - just create new value
	if source == nil {
		if kind == reflect.Map {
			return reflect.MakeMap(t)
		}
		return reflect.New(t)
	}

	// Get source reflect value
	value := reflect.ValueOf(source)

	// If source is already desired type - return its value
	if ValueTypeMatch(value, t) {
		return value
	}

	// Create new value of desired type
	if kind == reflect.Map {
		dest = reflect.MakeMap(t)
	} else {
		dest = reflect.New(t)
	}

	// Convert source to desired value
	ConvertValue(value, dest, t)

	return
}

// ConvertValue - Converts source into existing value of type `t`.
// Source value can be a struct or a map.
func ConvertValue(
	source reflect.Value, dest reflect.Value, t reflect.Type) {

	// If destination type is empty, determine it from destination value (feature)
	if t == nil {
		t = dest.Type()
	}

	// If type is a pointer, get it's element type.
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	// If source is a pointer get its element
	if source.Kind() == reflect.Ptr {
		source = source.Elem()
	}

	// If destination is a pointer get its element
	if dest.Kind() == reflect.Ptr {
		dest = dest.Elem()
	}

	// If source kind is pointer to a struct
	if sourceKind := source.Kind(); sourceKind == reflect.Struct {
		if k := t.Kind(); k == reflect.Struct {
			StructToStruct(source, dest, t)
		} else if k == reflect.Map {
			StructToMap(source, dest)
		}
	} else if sourceKind == reflect.Map {
		if k := t.Kind(); k == reflect.Struct {
			MapToStruct(source, dest)
		} else if k == reflect.Map {
			MapToMap(source, dest)
		}
	}
}

// TypeMatch - Check if value has a `t` Type.
// If value kind is a pointer, value element type is compared.
func TypeMatch(v interface{}, t reflect.Type) bool {
	return ValueTypeMatch(reflect.ValueOf(v), t)
}

// ValueTypeMatch - Check if value has a `t` Type.
// If value kind is a pointer, value element type is compared.
func ValueTypeMatch(v reflect.Value, t reflect.Type) bool {
	// Get value type
	vt := v.Type()

	// If type is a pointer, get it's element type.
	if vt.Kind() == reflect.Ptr {
		vt = vt.Elem()
	}

	return vt == t
}

// StructToStruct - Copies source fields to destination fields.
// `t` is a destination type.
func StructToStruct(source, dest reflect.Value, t reflect.Type) {

	// If destination type is empty, determine it from destination value (feature)
	if t == nil {
		t = dest.Type()
	}

	// Iterate over destination fields
	for i := 0; i < t.NumField(); i++ {
		// Get destination field
		field := t.Field(i)

		// Get source field
		src := source.FieldByName(field.Name)

		// If source has no field
		if !src.IsValid() {
			continue
		}

		setStructField(src, dest, field.Type, field.Name)
	}
}

// StructToMap - Copies struct to a map.
func StructToMap(source, dest reflect.Value) {
	// Iterate over source fields
	t := source.Type()
	for i := 0; i < t.NumField(); i++ {
		// Get source field
		field := t.Field(i)

		// Get source field value
		value := source.FieldByName(field.Name)

		key := reflect.ValueOf(field.Name)
		dest.SetMapIndex(key, value)
	}
}

// MapToStruct - Copies map to a struct.
func MapToStruct(source, dest reflect.Value) {
	// Get source keys
	for _, key := range source.MapKeys() {
		field := dest.FieldByName(key.String())
		value := source.MapIndex(key)

		// Pass if no field in structure
		if !field.IsValid() {
			continue
		}

		setStructField(value, dest, field.Type(), key.String())
	}
}

// TODO: It's all shitty here i wanna puke when i see this
func setStructField(value, dest reflect.Value, fieldType reflect.Type, fieldName string) {
	// Copy if types match
	if value.Type() == fieldType {
		// Copy source value to destination
		dest.FieldByName(fieldName).Set(value)
	} else {
		// If both are structs - make a deep copy
		srcKind := value.Kind()
		fieldKind := fieldType.Kind()
		// log.Printf("%s: %q, %q (%v)", fieldName, srcKind, fieldKind, value.CanInterface())

		if srcKind == reflect.Struct && srcKind == fieldKind {
			// Create a new value of desired type
			deep := reflect.New(fieldType)

			// Copy from source to new value
			ConvertValue(value, deep, fieldType)

			// Set deep value
			dest.FieldByName(fieldName).Set(deep.Elem())
		} else if srcKind == reflect.Map && srcKind == fieldKind {
			deep := reflect.MakeMap(fieldType)
			MapToMap(value, deep)
			dest.FieldByName(fieldName).Set(deep)
		} else if srcKind == reflect.Interface {
			setStructField(value.Elem(), dest, fieldType, fieldName)
		}
	}
}

// MapToMap - Copies map into another map.
func MapToMap(source, dest reflect.Value) {
	// Get source keys
	for _, key := range source.MapKeys() {
		value := source.MapIndex(key)
		dest.SetMapIndex(key, value)
	}
}
