package structs

import (
	"reflect"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestConvert(t *testing.T) {
	Convey("Should convert map to struct", t, func() {
		// Desired type
		type n struct {
			Test   string
			Number int
		}

		Convey("Interface map", func() {
			input := map[string]interface{}{"Test": "value", "Number": 100}
			// Convert value into desired type
			res := Convert(input, reflect.TypeOf(&n{})).Interface().(*n)
			So(res.Number, ShouldEqual, input["Number"])
			So(res.Test, ShouldEqual, input["Test"])
		})

		Convey("Typed map", func() {
			input := map[string]string{"Test": "value"}
			// Convert value into desired type
			res := Convert(input, reflect.TypeOf(&n{})).Interface().(*n)
			So(res.Test, ShouldEqual, input["Test"])
		})
	})

	Convey("Should convert struct to map", t, func() {
		type m map[string]interface{}
		var s1 = struct{ Test string }{Test: "value"}
		res := Convert(s1, reflect.TypeOf(m{})).Interface().(m)
		So(res["Test"], ShouldEqual, s1.Test)
	})

	Convey("Should convert struct to struct", t, func() {
		// Input type
		type i struct {
			Test, None string
			Deep       struct {
				None   int
				Test   string
				Deeper struct {
					None  string
					Test  string
					Other int
				}
			}
		}

		// Desired type
		type n struct {
			Test string
			None int
			Deep struct {
				None   string
				Test   string
				Deeper struct {
					None int
					Test string
					Even float64
				}
			}
		}

		// Input value
		var iV = &i{Test: "ok", None: "none"}
		iV.Deep.Test = "deep"
		iV.Deep.Deeper.Test = "deep"

		// Convert value into desired type
		t := reflect.TypeOf(&n{})
		v := Convert(iV, t)

		// Result structure
		r := v.Interface().(*n)

		So(v.Type(), ShouldEqual, t)
		So(r.Test, ShouldEqual, iV.Test)
		So(r.Deep.Test, ShouldEqual, iV.Deep.Test)
		So(r.Deep.Deeper.Test, ShouldEqual, iV.Deep.Deeper.Test)
	})

	// PASS
	Convey("Should not convert struct if already desired type", t, func() {
		// Desired type
		type n struct{ Test string }

		// Input value
		var s1 = &n{Test: "value"}

		// Convert value into desired type
		v := Convert(s1, reflect.TypeOf(s1))

		So(v.Interface(), ShouldEqual, s1)
	})

	// PASS
	Convey("Should create new struct if source is nil", t, func() {
		type n struct{ Test string }
		t := reflect.TypeOf(&n{})
		res := Convert(nil, t)
		So(res.Type(), ShouldEqual, t)
	})
}

func TestTypeMatch(t *testing.T) {
	Convey("Should match the types", t, func() {
		type n struct{ Test string }
		i := &n{}
		t := reflect.TypeOf(i).Elem()
		So(TypeMatch(i, t), ShouldBeTrue)
	})
}

func TestValueTypeMatch(t *testing.T) {
	Convey("Should match the types", t, func() {
		type n struct{ Test string }
		i := &n{}
		v := reflect.ValueOf(i)
		t := reflect.TypeOf(i).Elem()
		So(ValueTypeMatch(v, t), ShouldBeTrue)
	})
}

func BenchmarkConvert(b *testing.B) {
	// Input type
	type in struct {
		Test, None string
		Deep       struct {
			None   int
			Test   string
			Deeper struct {
				None string
				Test string
			}
		}
	}

	// Desired type
	type n struct {
		Test string
		None int
		Deep struct {
			None   string
			Test   string
			Deeper struct {
				None int
				Test string
			}
		}
	}

	t := reflect.TypeOf(&n{})

	for i := 0; i < b.N; i++ {
		// Input value
		var iV = &in{Test: "ok", None: "none"}
		iV.Deep.Test = "deep"
		iV.Deep.Deeper.Test = "deep"

		// Convert value into desired type
		Convert(iV, t)
	}
}

func BenchmarkStaticConvert(b *testing.B) {
	type in struct {
		Test, None string
		Deep       struct {
			None   int
			Test   string
			Deeper struct {
				None string
				Test string
			}
		}
	}

	for i := 0; i < b.N; i++ {
		// Input value
		var iV = &in{Test: "ok", None: "none"}
		iV.Deep.Test = "deep"
		iV.Deep.Deeper.Test = "deep"

		_ = map[string]interface{}{
			"Test": iV.Test,
			"None": iV.None,
			"Deep": map[string]interface{}{
				"Test": iV.Deep.Test,
				"Deeper": map[string]interface{}{
					"Test": iV.Deep.Deeper.Test,
				},
			},
		}
	}
}
