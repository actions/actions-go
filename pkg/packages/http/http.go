package http

import (
	"io/ioutil"
	"net/http"

	"bitbucket.org/actions/actions-go/pkg/services"
)

// HTTP - HTTP service.
type HTTP struct{}

// Request - HTTP Request.
type Request struct {
	Method string `default:"GET"`
	Scheme string `default:"http"`
	URL    string
	Path   string
	Host   string
	Header http.Header
	Body   []byte
}

// Response - HTTP Response.
type Response struct {
	Status int
	Header http.Header
	Body   []byte
}

// Request - Makes a http request.
func (h *HTTP) Request(req *Request, resp *Response) (err error) {
	response, err := http.Get(req.URL)
	if err != nil {
		return
	}

	defer response.Body.Close()
	resp.Body, err = ioutil.ReadAll(response.Body)

	return
}

func init() {
	services.Register(&HTTP{}, "http", "v1", "HTTP Service")
}
