package json

import (
	"encoding/json"

	"bitbucket.org/actions/actions-go/pkg/services"
)

// JSON - JSON service.
type JSON struct{}

// Serialized - Serialized request.
type Serialized struct {
	Body []byte
}

// Serialize - Serializes a request.
func (j *JSON) Serialize(req services.M, resp *Serialized) (err error) {
	resp.Body, err = json.MarshalIndent(req, "", "    ")
	return
}

func init() {
	services.Register(&JSON{}, "json", "v1", "JSON Service")
}
