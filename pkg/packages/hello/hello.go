package hello

import (
	"fmt"

	"bitbucket.org/actions/actions-go/pkg/services"
)

// Hello - Hello service.
type Hello struct{}

// Request - Hello Request.
type Request struct {
	Name string
}

// Response - Hello Response.
type Response struct {
	Hello string
}

// Say - Says hello.
func (h *Hello) Say(req *Request, resp *Response) error {
	if req.Name == "" {
		req.Name = "World"
	}

	resp.Hello = fmt.Sprintf("Hello %s!", req.Name)

	return nil
}

func init() {
	services.Register(&Hello{}, "hello", "v1", "Hello Service")
}
