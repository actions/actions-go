package html

import (
	"errors"
	"fmt"
	"strings"
	"text/template"

	"github.com/moovweb/gokogiri"
	"github.com/moovweb/gokogiri/xml"
	"github.com/moovweb/gokogiri/xpath"

	"bytes"

	"bitbucket.org/actions/actions-go/pkg/services"
)

// HTML - HTML Service.
type HTML struct{}

// ExtractRequest -
type ExtractRequest struct {
	Selectors services.M `required:"true"`
	Body      []byte     `required:"true"`
}

// Extract - Extracts from html.
func (h *HTML) Extract(req *ExtractRequest, resp services.M) (err error) {
	doc, err := gokogiri.ParseHtml(req.Body)
	if err != nil {
		return
	}
	defer doc.Free()

	root := doc.Root()
	if root == nil {
		return fmt.Errorf("Document root was nil")
	}
	defer root.Free()

	// Extract
	for key, selector := range req.Selectors {
		s, ok := selector.(string)
		if !ok {
			continue
		}

		value, err := searchXpath(root, s)
		if err != nil {
			return err
		}

		resp[key] = value
	}

	return
}

// RenderResponse - Rendered html.
type RenderResponse struct {
	Body []byte
}

// Render - Renders html template.
func (h *HTML) Render(req services.M, resp *RenderResponse) (err error) {
	tmp, ok := req.Get("Template").(string)
	if !ok {
		return errors.New("No template was set")
	}

	templ, err := template.New("").Parse(tmp)
	if err != nil {
		return
	}

	var out bytes.Buffer
	err = templ.Execute(&out, req)
	if err != nil {
		return
	}

	resp.Body = out.Bytes()

	return
}

func searchXpath(node xml.Node, path string) (match string, err error) {
	// Ensure normalize-space is used
	if !strings.Contains(path, "normalize-space") {
		path = fmt.Sprintf("normalize-space(%s)", path)
	}

	// Compile expression
	expression := xpath.Compile(path)
	if expression == nil {
		err = fmt.Errorf("Invalid XPath expression: %s", path)
		return
	}
	defer expression.Free()

	x := xpath.NewXPath(node.NodePtr())
	defer x.Free()

	if err = x.Evaluate(node.NodePtr(), expression); err != nil {
		return
	}

	match, err = x.ResultAsString()
	match = strings.TrimSpace(match)
	return
}

func init() {
	services.Register(&HTML{}, "html", "v1", "HTML Service")
}
