package services

// Sequence -
type Sequence []struct {
	Name string `json:",omitempty"`
	Args M      `json:",omitempty"`
}
