package services

import (
	"reflect"
	"strings"
	"unicode"
	"unicode/utf8"

	"bitbucket.org/actions/actions-go/pkg/structs"
)

// Method -
type Method struct {
	ReqType  reflect.Type
	RespType reflect.Type
	required []string
	method   *reflect.Method
	info     *MethodInfo
	svc      *Service
}

// newMethod - Creates a service method.
func newMethod(svc *Service, m *reflect.Method) *Method {
	t := m.Type

	// Method needs to be exported
	// And have 3 in params and one out parameter (error)
	if m.PkgPath != "" || t.NumIn() != 3 || t.NumOut() != 1 {
		return nil
	}

	// Check either all in parameters are exported
	for i := 0; i < t.NumIn(); i++ {
		if in := t.In(i); !isExportedOrBuiltin(in) {
			return nil
		}
	}

	// Get request type
	reqType, ok := getType(t.In(1))
	if !ok {
		return nil
	}

	// Get response type
	respType, ok := getType(t.In(2))
	if !ok {
		return nil
	}

	// Create a method
	method := &Method{
		ReqType:  reqType,
		RespType: respType,
		method:   m,
		svc:      svc,
	}

	// Method information
	method.info = &MethodInfo{
		Name: strings.ToLower(m.Name),
	}

	return method
}

// Call - Calls a method.
func (m *Method) Call(req, resp interface{}) error {
	var (
		reqValue  = getValue(req, m.ReqType)
		respValue = getValue(resp, m.RespType)
	)

	// Call function
	errValue := m.method.Func.Call([]reflect.Value{
		m.svc.rcvr,
		reqValue,
		respValue,
	})

	// Check error
	if err := errValue[0].Interface(); err != nil {
		return err.(error)
	}

	// Copy response if created new structure
	if respValue.Type() != m.RespType {
		structs.ConvertValue(respValue, reflect.ValueOf(resp), nil)
	}

	return nil
}

// Info - Returns service information.
func (m *Method) Info() *MethodInfo {
	return m.info
}

func getType(t reflect.Type) (reflect.Type, bool) {
	switch t.Kind() {
	case reflect.Ptr:
		return getType(t.Elem())
	case reflect.Struct, reflect.Map:
		return t, true
	default:
		return nil, false
	}
}

// getValue - Gets value of `v` in type `t`.
func getValue(v interface{}, t reflect.Type) reflect.Value {
	switch v.(type) {
	case reflect.Value:
		return v.(reflect.Value)
	}

	// If value is empty - create a new one
	if v == nil {
		// If its a map, create a map
		if t.Kind() == reflect.Map {
			return reflect.MakeMap(t)
		}

		// Otherwise create other
		return reflect.New(t)
	}

	// If value kind is as desired, return it
	k := reflect.TypeOf(v)
	if k.Kind() == reflect.Ptr && k.Elem() == t {
		return reflect.ValueOf(v)
	} else if k == t {
		return reflect.ValueOf(v)
	}

	// Otherwise convert to desired
	return structs.Convert(v, t)
}

func isExported(name string) bool {
	rune, _ := utf8.DecodeRuneInString(name)
	return unicode.IsUpper(rune)
}

func isExportedOrBuiltin(t reflect.Type) bool {
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	// PkgPath will be non-empty even for an exported type,
	// so we need to check the type name as well.
	return isExported(t.Name()) || t.PkgPath() == ""
}
