package services

import (
	"fmt"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCall(t *testing.T) {
	Convey("Should call a method with request pointer", t, func() {
		var req = struct{ Do string }{Do: "response"}
		res := struct{ Some string }{}
		err := Call("myservice.testmethod", &req, &res)
		So(err, ShouldEqual, nil)
		So(res.Some, ShouldEqual, "ok response")
	})

	Convey("Should call a method and get adequate error", t, func() {
		var req = struct{ Do string }{Do: "response"}
		res := struct{ Some string }{}
		err := Call("myservice.errormethod", &req, &res)
		So(err, ShouldEqual, errService)
	})

	Convey("Should call a method with no pointer request", t, func() {
		var req = struct{ Do string }{Do: "response"}
		res := struct{ Some string }{}
		err := Call("myservice.testmethod", req, &res)
		So(err, ShouldEqual, nil)
		So(res.Some, ShouldEqual, "ok response")
	})

	Convey("Should call a method with no request", t, func() {
		res := struct{ Some string }{}
		err := Call("myservice.testmethod", nil, &res)
		So(err, ShouldEqual, nil)
		So(res.Some, ShouldEqual, "ok ")
	})

	Convey("Should not construct new resp struct if not needed", t, func() {
		req := &MyRequest1{Do: "some"}
		res := &MyResponse1{}
		err := Call("myservice.testmethod", req, res)
		So(err, ShouldEqual, nil)
		So(res.Some, ShouldEqual, "ok some")
	})

	Convey("Should return error if not method was found", t, func() {
		res := struct{ Some string }{}
		err := Call("myservice.nomethod", nil, &res)
		want := fmt.Sprintf("Not Found: method %q was not found", "myservice.nomethod")
		So(err.Error(), ShouldEqual, want)
	})

}

// BenchmarkMethodCall	  500000	      4071 ns/op
func BenchmarkMethodCall(b *testing.B) {
	for i := 0; i < b.N; i++ {
		req := struct{ Do string }{Do: "response"}
		res := struct{ Some string }{}
		Call("myservice.testmethod", &req, &res)
	}
}
