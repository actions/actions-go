package services

// DefaultRegistry - Default services registry.
var DefaultRegistry = NewRegistry()

// Register - Registers a service in default registry.
// Panic's if something goes wrong.
func Register(service interface{}, name, ver, title string) {
	DefaultRegistry.Register(service, name, ver, title)
}

// ServiceByName - Finds service by name or ID.
func ServiceByName(name string) *Service {
	return DefaultRegistry.ServiceByName(name)
}

// MethodByName - Finds service method by name.
func MethodByName(name string) *Method {
	return DefaultRegistry.MethodByName(name)
}

// Services - Returns all services in registry.
func Services() []*Service {
	return DefaultRegistry.Services()
}

// Call - Calls service method.
func Call(name string, req, resp interface{}) error {
	return DefaultRegistry.Call(name, req, resp)
}

// RunSequence - Runs a sequence of calls.
func RunSequence(s Sequence, req, resp interface{}) error {
	return DefaultRegistry.RunSequence(s, req, resp)
}
