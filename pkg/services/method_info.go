package services

// MethodInfo -
type MethodInfo struct {
	Name        string // eq. get (cache.get)
	Description string
}
