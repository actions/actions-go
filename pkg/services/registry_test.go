package services

import (
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

// MyRequest1 -
type MyRequest1 struct{ Do string }

// MyResponse1 -
type MyResponse1 struct{ Some string }

// MyService - Test service.
type MyService struct{}

// TestMethod -
func (s *MyService) TestMethod(req *MyRequest1, resp *MyResponse1) error {
	resp.Some = "ok " + req.Do
	return nil
}

var errService = errors.New("service error")

// ErrorMethod -
func (s *MyService) ErrorMethod(req *MyRequest1, resp *MyResponse1) error {
	return errService
}

// NotMethod -
func (s *MyService) NotMethod(req MyRequest1, resp MyResponse1) error   { return nil }
func (s *MyService) nonMethod(req *MyRequest1, resp *MyResponse1) error { return nil }

func TestServiceByName(t *testing.T) {
	Convey("Should find service by name", t, func() {
		service := ServiceByName("myservice")
		So(service, ShouldNotEqual, nil)
		So(service.Info(), ShouldNotEqual, nil)
		So(service.Info().Name, ShouldEqual, "myservice")
		So(len(service.Methods()), ShouldEqual, 3)
	})

	Convey("Should find service by ID", t, func() {
		service := ServiceByName("myservice:v1")
		So(service, ShouldNotEqual, nil)
		So(service.Info(), ShouldNotEqual, nil)
		So(service.Info().Name, ShouldEqual, "myservice")
		So(service.Info().Version, ShouldEqual, "v1")
	})

	Convey("Should not find non-existent service", t, func() {
		service := ServiceByName("noservice")
		So(service, ShouldEqual, nil)
	})
}

func TestMethodByName(t *testing.T) {
	Convey("Should find service method by name", t, func() {
		method := MethodByName("myservice:v1.testmethod")
		So(method, ShouldNotEqual, nil)
		So(method.Info(), ShouldNotEqual, nil)
		So(method.Info().Name, ShouldEqual, "testmethod")
	})

	Convey("Should return nil if service doesnt exists", t, func() {
		method := MethodByName("myservice:v2.testmethod")
		So(method, ShouldEqual, nil)
	})

	Convey("Should return nil if method doesnt exists", t, func() {
		method := MethodByName("myservice:v1.testmethod2")
		So(method, ShouldEqual, nil)
	})
}

func TestServices(t *testing.T) {
	Convey("Should return all services", t, func() {
		So(len(Services()), ShouldEqual, 1)
	})
}

func init() {
	Register(&MyService{}, "myservice", "v1", "My Test Service")
}
