package services

import (
	"fmt"
	"reflect"
	"strings"

	"bitbucket.org/actions/actions-go/pkg/structs"
)

// Registry - Services registry.
type Registry struct {
	services []*Service
}

// NewRegistry - Creates a new services registry.
func NewRegistry() *Registry {
	return &Registry{
		services: []*Service{},
	}
}

// Register - Registers a service. Panic's if something goes wrong.
func (r *Registry) Register(service interface{}, name, ver, title string) {
	svc := NewService(service, name, ver, title)
	r.services = append(r.services, svc)
}

// ServiceByName - Finds service by name or ID.
func (r *Registry) ServiceByName(name string) *Service {
	checkID := strings.ContainsAny(name, ":")
	for _, service := range r.services {
		// If is ID check id
		if checkID && service.info.ID == name {
			return service
		}

		// Check name
		if !checkID && service.info.Name == name {
			return service
		}
	}
	return nil
}

// MethodByName - Finds service method by name.
func (r *Registry) MethodByName(name string) *Method {
	serviceName, methodName := splitMethodName(name)
	service := r.ServiceByName(serviceName)
	if service == nil {
		return nil
	}

	return service.MethodByName(methodName)
}

// Call - Calls service method.
func (r *Registry) Call(name string, req, resp interface{}) error {
	method := r.MethodByName(name)
	if method == nil {
		return fmt.Errorf("Not Found: method %q was not found", name)
	}
	return method.Call(req, resp)
}

// RunSequence - Runs a sequence of calls.
func (r *Registry) RunSequence(s Sequence, req, resp interface{}) error {
	var (
		request  M
		response M
	)

	// Copy req to request
	if req != nil {
		request = make(M)
		structs.ConvertValue(
			reflect.ValueOf(req),
			reflect.ValueOf(request),
			nil,
		)
	}

	// Iterate over sequence
	for _, call := range s {
		// Request
		if len(request) == 0 {
			if len(response) == 0 {
				request = call.Args
			} else if len(call.Args) == 0 {
				request = response
			} else {
				request = make(M)
				request.Merge(call.Args)
				request.Merge(response)
			}
		} else {
			request.Defaults(call.Args)
		}

		// Create new response
		response = make(M)

		// Call service
		err := Call(call.Name, request, response)
		if err != nil {
			return err
		}

		// Purge request
		request = nil
	}

	if len(response) > 0 && resp != nil {
		structs.ConvertValue(
			reflect.ValueOf(response),
			reflect.ValueOf(resp),
			nil,
		)
	}

	return nil
}

// Services - Returns all services in registry.
func (r *Registry) Services() []*Service {
	return r.services
}

// splitMethodName - Splits a method name in format `service:v1.method` into `service:v1`, `method`.
func splitMethodName(name string) (serviceName string, methodName string) {
	f := strings.IndexByte(name, '.')
	if f >= 0 {
		serviceName = name[:f]
		methodName = name[f+1:]
	}
	return
}
