package services

// M - Type of input and output of an action.
type M map[string]interface{}

// Has - Checks existence of key in a map.
func (m M) Has(key string) bool {
	_, has := m[key]
	return has
}

// Get - Returns value from map by key.
func (m M) Get(key string) interface{} {
	return m[key]
}

// Set - Sets value in map under key.
func (m M) Set(key string, value interface{}) {
	m[key] = value
}

// Merge - Merges map with another and returns current map.
func (m M) Merge(other M) M {
	for key, value := range other {
		m[key] = value
	}
	return m
}

// Defaults - Sets defaults.
func (m M) Defaults(other M) M {
	for key, value := range other {
		if _, ok := m[key]; !ok {
			m[key] = value
		}
	}
	return m
}

// Copy - Creates a copy of a map.
func (m M) Copy() M {
	context := M{}
	context.Merge(m)
	return context
}

// Extend - Copies context and merges other into the copy.
func (m M) Extend(other M) M {
	return m.Copy().Merge(other)
}
