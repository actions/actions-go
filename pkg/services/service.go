package services

import (
	"fmt"
	"reflect"
)

// Service -
type Service struct {
	rcvr     reflect.Value
	rcvrType reflect.Type
	info     *ServiceInfo
	methods  map[string]*Method
}

// NewService - Constructs a new service.
func NewService(s interface{}, name, version, title string) *Service {
	service := &Service{
		rcvr:     reflect.ValueOf(s),
		rcvrType: reflect.TypeOf(s),
		methods:  make(map[string]*Method),
	}

	// Construct service methods
	for i := 0; i < service.rcvrType.NumMethod(); i++ {
		m := service.rcvrType.Method(i)
		if method := newMethod(service, &m); method != nil {
			service.methods[m.Name] = method
		}
	}

	// Service information
	service.info = &ServiceInfo{
		ID:      fmt.Sprintf("%s:%s", name, version),
		Name:    name,
		Version: version,
		Title:   title,
	}

	return service
}

// Methods - Returns all service methods.
func (s *Service) Methods() map[string]*Method {
	return s.methods
}

// MethodByName - Finds method by name.
func (s *Service) MethodByName(name string) *Method {
	for _, method := range s.methods {
		if method.info.Name == name {
			return method
		}
	}
	return nil
}

// Info - Returns service information.
func (s *Service) Info() *ServiceInfo {
	return s.info
}
