package services

// ServiceInfo -
type ServiceInfo struct {
	ID          string // eq. cache:v1
	Name        string // eq. cache
	Version     string // eq. v1
	Title       string // eq. Cache Service
	Description string
}
