package apis

import (
	"fmt"
	"net/http"

	"bitbucket.org/actions/actions-go/pkg/remote"
)

// API -
type API struct {
	Hostname string   `json:"hostname,omitempty"`
	Routes   []*Route `json:"routes,omitempty"`
}

// NewAPI - Creates new API.
func NewAPI(hostname string) *API {
	return &API{Hostname: hostname}
}

// RegisterRoute - Registers a new route.
func (a *API) RegisterRoute(method, path string, action *Action) *Route {
	route := &Route{
		Method: method,
		Path:   path,
		Action: action,
	}
	a.Routes = append(a.Routes, route)
	return route
}

// ServeHTTP - HTTP handler.
func (a *API) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	route := a.getRoute(r.Method, r.URL.Path)
	if route == nil {
		err := fmt.Errorf("Route %s %s not found", r.Method, r.URL.Path)
		remote.WriteError(w, http.StatusNotFound, err)
		return
	}

	// Serve the route
	route.ServeHTTP(w, r)
}

func (a *API) getRoute(method, path string) *Route {
	for _, r := range a.Routes {
		if r.Path == path && (r.Method == method || r.Method == "") {
			return r
		}
	}
	return nil
}
