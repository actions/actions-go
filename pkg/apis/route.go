package apis

import (
	"net/http"

	"bitbucket.org/actions/actions-go/pkg/remote"
	"bitbucket.org/actions/actions-go/pkg/services"
)

// Route - HTTP route.
// It can execute action or a sequence of actions.
//
// A response is written depending by `Response`.
type Route struct {
	Path     string            `json:""`
	Method   string            `json:",omitempty"`
	Action   *Action           `json:",omitempty"`
	Sequence services.Sequence `json:",omitempty"`
	Response *ResponseTypes    `json:",omitempty"`
}

// Action - Service method call, where Name is a service method name
// and Args are call arguments.
type Action struct {
	Name string     `json:",omitempty"`
	Args services.M `json:",omitempty"`
}

// ServeHTTP - HTTP handler.
// Calls route Sequence or Action then writes Response.
//
// TODO: Request pre-processor.
func (r *Route) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	response := make(services.M)
	err := r.callAction(req, response)
	if err != nil {
		remote.WriteError(w, http.StatusInternalServerError, err)
		return
	}

	// Write response
	if r.Response == nil {
		DefaultResponseType.WriteResponse(w, req, response)
	} else {
		r.Response.WriteResponse(w, req, response)
	}
}

// callAction - Calls action or a sequence.
func (r *Route) callAction(req *http.Request, response interface{}) (err error) {
	if r.Sequence == nil {
		err = services.Call(r.Action.Name, r.Action.Args, response)
	} else {
		err = services.RunSequence(r.Sequence, nil, response)
	}
	return
}
