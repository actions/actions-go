package apis

import (
	"net/http"
	"strings"

	"bitbucket.org/actions/actions-go/pkg/remote"
	"bitbucket.org/actions/actions-go/pkg/services"
)

// ResponseTypes - Describes route response type, determined by `Accept` header.
//
// Default response type used when no `Accept` field is set or
// no response type was determined by `Accept` header.
// If not set it's `DefaultResponseType`.
//
// `Accept` field is a map of Response Types by `Accept` header.
type ResponseTypes struct {
	Default *ResponseType            `json:",omitempty"`
	Accept  map[string]*ResponseType `json:",omitempty"`
}

// ResponseType - Response type describes which attributes are send as response
// and how they are serialized.
//
// `ContentType` field is a response `Content-Type` header.
//
// Field `Attributes` determines which attributes will be send back as response.
// If it's not set - one `Attribute` can be send.
// If none is set, full result will be send.
//
// `PostProcessor` field is an action that
// will be ran after the route action with its result as arguments.
// For example it can be `json.serialize` or `html.render` action.
// If it is one of them, remember to set `Attribute` value to "Body".
type ResponseType struct {
	ContentType   string  `json:",omitempty"`
	Attribute     string  `json:",omitempty"`
	PostProcessor *Action `json:",omitempty"`
}

// DefaultResponseType -
var DefaultResponseType = &ResponseType{
	ContentType:   "application/json",
	PostProcessor: DefaultPostProcessor,
}

// DefaultPostProcessor -
var DefaultPostProcessor = &Action{
	Name: "json.serialize",
}

// WriteResponse -
func (r *ResponseTypes) WriteResponse(
	w http.ResponseWriter, req *http.Request, response services.M) {

	// Get `Accept` Request header
	// TODO: Get accept content-type
	accept := parseAccept(req.Header.Get("Accept"))

	// Get request type by accept header
	if t, ok := r.Accept[accept]; ok {
		t.WriteResponse(w, req, response)
		return
	}

	// If default response type is not set, use globally default
	if r.Default == nil {
		r.Default = DefaultResponseType
	}

	// Send response using default response type
	r.Default.WriteResponse(w, req, response)
}

// WriteResponse -
func (r *ResponseType) WriteResponse(
	w http.ResponseWriter, req *http.Request, response services.M) {

	// Write Content-Type header
	if r.ContentType != "" {
		w.Header().Set("Content-Type", r.ContentType)
	}

	// Run post processor
	if r.PostProcessor != nil {
		runPostProcessor(w, req, r.PostProcessor, response)
	} else {
		runPostProcessor(w, req, DefaultPostProcessor, response)
	}
}

func runPostProcessor(
	w http.ResponseWriter, req *http.Request, p *Action, response services.M) {

	// TODO: use post processor arguments
	var resp = &struct{ Body []byte }{}
	args := response.Defaults(p.Args)
	err := services.Call(p.Name, args, resp)
	if err != nil {
		remote.WriteError(w, http.StatusInternalServerError, err)
	}

	w.Write(resp.Body)
}

func parseAccept(h string) string {
	f := strings.Index(h, ",")
	if f == -1 {
		return h
	}
	return h[:f]
}
