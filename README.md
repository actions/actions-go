## What is the purpose of this code-being

In simple words and simple code.

It's intended to make remote sequences of calls to so-called microservices,
construct RESTful APIs for them and probably much more.

```Go
// proto/2/main.go
package main

import (
  "log"

  _ "bitbucket.org/actions/actions-go/pkg/packages/html"
  _ "bitbucket.org/actions/actions-go/pkg/packages/http"
  "bitbucket.org/actions/actions-go/pkg/services"
  _ "bitbucket.org/actions/actions-go/proto/1/svc"
)

func main() {
  var titleResponse = struct{ Title string }{}
  var seq = services.Sequence{
    {"http.request", services.M{"URL": "http://google.com/"}},
    {"html.extract", services.M{"Selectors": services.M{"Title": "//title/text()"}}},
  }

  // Run sequence
  err := services.RunSequence(seq, nil, &titleResponse)
  if err != nil {
    log.Fatal(err)
  }

  log.Printf("Got title: %q", titleResponse.Title)
}
```

This library is in active development, is not ready for production
and will probably change dramatically.
